################################################################################
# Globals
################################################################################

# Include the Trilinos export makefiles from needed packages
TRILINOS_INSTALL_DIR = /home/users/mschpc/2017/harperi/Modules/ParallelNumericalAlgorithms/Assignment3/Trilinos-build
include $(TRILINOS_INSTALL_DIR)/include/Makefile.export.Epetra
include $(TRILINOS_INSTALL_DIR)/include/Makefile.export.EpetraExt
include $(TRILINOS_INSTALL_DIR)/include/Makefile.export.Teuchos
include $(TRILINOS_INSTALL_DIR)/include/Makefile.export.Belos

# Add the Trilinos installation directory to the search paths for libraries and headers
LIB_PATH = -L$(TRILINOS_INSTALL_DIR)/lib 
           
INCLUDES = -I. -I$(TRILINOS_INSTALL_DIR)/include 

# Add the Trilinos libraries, search path, and rpath to the
# linker command line arguments
LIBS =  $(Epetra_SHARED_LIB_RPATH_COMMAND) \
        $(Epetra_LIBRARIES) \
        $(EpetraExt_LIBRARIES) \
        $(Teuchos_LIBRARIES) \
        $(Belos_LIBRARIES) \
        $(Epetra_TPL_LIBRARIES) \
        $(Epetra_EXTRA_LD_FLAGS)

# Set the C++ compiler and flags to those specified in the export makefile
CXX = $(Epetra_CXX_COMPILER)
CXXFLAGS = $(Epetra_CXX_FLAGS) -m64 -g

all:	run

MATRIX   = "atmosmodd.mtx" #Matrix filename
NVECS1    = "2"          #size of block of vectors to test
NVECS2    = "5"          #size of block of vectors to test
NVECS3    = "10"          #size of block of vectors to test
NMATVECS = "100"         #numer of tests to perform to get timing averages
NPROCS = "2"             #numer of MPI processes
nvecs = 2
nprocs = 16 
################################################################################
# Build Test
################################################################################
#&> $(nvecs)blocks_$(nprocs)procs_results.txt

runEpetra:    MatVecTestEpetra.exe
	mpirun -n $(NPROCS) MatVecTestEpetra.exe --filename=$(MATRIX)  --numVecs=$(NVECS1) --numMatVecs=$(NMATVECS) 

MatVecTestEpetra.exe:	MatVecTestEpetra.o
	$(CXX) $(INCLUDES) $(CXXFLAGS) -o MatVecTestEpetra.exe MatVecTestEpetra.o $(LDFLAGS) $(CXXFLAGS) $(LIB_PATH) $(LIBS) 

MatVecTestEpetra.o:	MatVecTestEpetra.cpp
	$(CXX) $(INCLUDES) $(CXXFLAGS) -c -o MatVecTestEpetra.o ./MatVecTestEpetra.cpp 

################################################################################
# GMRES
################################################################################
#&> $(nvecs)blocks_$(nprocs)procs_results.txt

runGMRES:    GMRES.exe
	mpirun -n $(nprocs) GMRES.exe --filename="ford2.mtx" 

runAllGMRES: GMRES.exe
	mpirun -n 2 GMRES.exe --verbose --block-size=$(NVECS1)  --filename="smallworld.mtx"
	mpirun -n 4 GMRES.exe --verbose --block-size=$(NVECS1) --filename="smallworld.mtx"
	mpirun -n 8 GMRES.exe --verbose --block-size=$(NVECS1) --filename="smallworld.mtx"
	mpirun -n 16 GMRES.exe --verbose --block-size=$(NVECS1) --filename="smallworld.mtx"
	mpirun -n 2 GMRES.exe --verbose --block-size=$(NVECS2)  --filename="smallworld.mtx"
	mpirun -n 4 GMRES.exe --verbose --block-size=$(NVECS2) --filename="smallworld.mtx"
	mpirun -n 8 GMRES.exe --verbose --block-size=$(NVECS2) --filename="smallworld.mtx"
	mpirun -n 16 GMRES.exe --verbose --block-size=$(NVECS2) --filename="smallworld.mtx"
	mpirun -n 2 GMRES.exe --verbose --block-size=$(NVECS3)  --filename="smallworld.mtx"
	mpirun -n 4 GMRES.exe --verbose --block-size=$(NVECS3) --filename="smallworld.mtx"
	mpirun -n 8 GMRES.exe --verbose --block-size=$(NVECS3) --filename="smallworld.mtx"
	mpirun -n 16 GMRES.exe --verbose --block-size=$(NVECS3) --filename="smallworld.mtx"

GMRES.exe: GMRES.o
	$(CXX) $(INCLUDES) $(CXXFLAGS) -o GMRES.exe GMRES.o $(LDFLAGS) $(CXXFLAGS) $(LIB_PATH) $(LIBS) 

GMRES.o: BlockGmresEpetraExFile.cpp
	$(CXX) $(INCLUDES) $(CXXFLAGS) -c -o GMRES.o ./BlockGmresEpetraExFile.cpp 

################################################################################
# Clean
################################################################################

clean:
	rm -rf core *.o *.exe out.txt
	clear
