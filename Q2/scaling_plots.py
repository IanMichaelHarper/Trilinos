import pylab as plt
import pandas as pd

num_procs = [2,4,8,16]


df = pd.read_csv("pre2_results.csv")

#plot residual against number of processors
plt.plot(df["Residual"],df["NumProcs"],'o', linestyle='-') 
plt.xlabel("Number of Processors")
plt.ylabel("Residual")
plt.title("Final Residual vs. Number of Processors for pre2.mtx")
#plt.legend(loc="upper left", prop={'size':10})
plt.savefig("pre2_block_graph.png")

