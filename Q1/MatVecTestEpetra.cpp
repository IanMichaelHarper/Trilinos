#include <sys/time.h>
#include <fstream>
#include "BelosConfigDefs.hpp"
#include "BelosEpetraAdapter.hpp"
#include "BelosLinearProblem.hpp"
#include "Epetra_CrsMatrix.h"
#include "Epetra_Map.h"
#include "Epetra_MultiVector.h"
#include "Epetra_SerialComm.h"
#include "Epetra_MpiComm.h"
#include "Epetra_Vector.h"
#include "EpetraExt_CrsMatrixIn.h"
#include "Teuchos_CommandLineProcessor.hpp"
#include "Teuchos_Exceptions.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include "Trilinos_Util.h"

// Global Timers
Teuchos::RCP<Teuchos::Time> BlockTime = Teuchos::TimeMonitor::getNewCounter("Block Time");
Teuchos::RCP<Teuchos::Time> SingleTime = Teuchos::TimeMonitor::getNewCounter("Single Time");
/*
char * results_filename(std::string filename){
	int n = 32;
	char * buffer = new char[n]; //filename surely won''t be longer than 32 letters
	for (int i=0; i<n; i++){
		buffer[i] = filename[i]; //start saving filename to buffer
		if (filename[i] == '.'){ //when we hit the extension
			dot = i; //save the spot
			break; 
		}
	}
	char * results_ext[n];
	results_ext = ['_','r','e','s','u','l','t','s','.','c','s','v'];
	int results_ext_size = 12;
	for (int i=0; i<results_ext_size; i++){
		buffer[dot+i] = results_ext[i]; //overwrite dot with underscore
	}
	return buffer;
}
*/
int main(int argc, char *argv[]) {

#ifdef EPETRA_MPI
  // Initialize MPI
  MPI_Init(&argc,&argv);
  int size,rank; 
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  Epetra_MpiComm Comm(MPI_COMM_WORLD);
#endif

  // Some default parameters
  std::string filename("gr_30_30.hb"); // default input filename
  int numVecs     = 1;
  int numMatVecs = 100;

  // Get parameters from command line
  Teuchos::CommandLineProcessor cmdp(false,true);
  cmdp.setOption("filename",&filename,"Matrix filename.");
  cmdp.setOption("numVecs",&numVecs,"Number of vectors in block matvec.");
  cmdp.setOption("numMatVecs",&numMatVecs,"Number of matrix-vector products to compute.");
  if (cmdp.parse(argc,argv) != Teuchos::CommandLineProcessor::PARSE_SUCCESSFUL) {
    return -1;
  }

//  char * results_filename = results_filename(filename);
  //std::cout << results_filename << endl;	
//  std::cout << filename.substr(filename.length()-4, ) + "_results.csv" << std::endl;
  // Allocate pointers to A,X,B
  Teuchos::RCP<Epetra_CrsMatrix> A;
  Teuchos::RCP<Epetra_MultiVector> B, X;
  Teuchos::RCP<Epetra_Vector> b;


  std::cout << filename + "\n";

  // Load A
  Epetra_CrsMatrix *Aptr = NULL;
  int retval = EpetraExt::MatrixMarketFileToCrsMatrix(filename.c_str(), Comm, Aptr); 
  TEUCHOS_TEST_FOR_EXCEPT_MSG(retval != 0, "Matrix read failed. Aborting.");
  A = Teuchos::rcp(Aptr);


  typedef double                          ST;
  typedef Epetra_Operator                 OP;
  typedef Epetra_MultiVector              MV;
  typedef Belos::OperatorTraits<ST,MV,OP> OPT;
  typedef Belos::MultiVecTraits<ST,MV>    MVT;

  X = Teuchos::rcp( new Epetra_MultiVector( A->Map(), numVecs ) );
  b = Teuchos::rcp( new Epetra_Vector( A->Map() ) );
  B = Teuchos::rcp( new Epetra_MultiVector( A->Map(), numVecs ) );
  MVT::MvRandom( *X );

  // prefetch
  OPT::Apply( *A, *X, *B );

  struct timeval start, end;
  long long tot;
  // Collect block timing data
  {
  // Construct local time monitor; will stop when leaving scope
  Teuchos::TimeMonitor LocalTimer(*BlockTime);
  gettimeofday(&start, NULL);
  for(int i=0;i<numMatVecs;i++) {
    OPT::Apply( *A, *X, *B );
  }
  gettimeofday(&end, NULL);
  }
  tot = (end.tv_sec - start.tv_sec)*1000000L + (end.tv_usec - start.tv_usec); 
  // Collect single vector timing data
  {
  // Construct local time monitor; will stop when leaving scope
  Epetra_Vector *x;
  Teuchos::TimeMonitor LocalTimer(*SingleTime);
  for(int i=0;i<numMatVecs;i++) {
    for(int j=0; j<numVecs; j++) {
      x = (*X)(j);
      OPT::Apply( *A, *x, *b );
    }
  }
  }

  double blockTime  = BlockTime->totalElapsedTime();
  blockTime = blockTime / (double)numMatVecs;
  double singleTime = SingleTime->totalElapsedTime();
  singleTime = singleTime / (double)numMatVecs;
  double AvgBlockTime=0; //average block time
  double AvgSingleTime=0; //average single time
  // Get a summary from the time monitor.
  Teuchos::TimeMonitor::summarize();
  std::cout << "blockTime = " << blockTime << std::endl; 
  std::cout << "gettimeofday time = " << tot << std::endl; 
/*
  for(int j=0; j<size; j++) {
    if (rank == j){
	  // Output average time
	  std::cout << std::endl << std::endl;
	  std::cout << "Process " << rank << std::endl;
	  std::cout << "Average time for matvec against block of vectors (multivector format): " << blockTime << std::endl;
	  std::cout << "Average time for matvec against block of vectors (single vector format): " << singleTime << std::endl;
	  std::cout << std::endl << std::endl;

	  AvgBlockTime += AvgBlockTime + blockTime;
	  AvgSingleTime += AvgSingleTime + singleTime;
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }*/
  AvgBlockTime = AvgBlockTime/size;
  AvgSingleTime = AvgSingleTime/size;
  if (rank == 0){ //need to get mean time of all processes from TimeMonitor, not just rank 0. 
	  //write to csv file to be used for plotting
	  std::ofstream myfile;
	 // myfile.open((filename.substr(filename.length()-4) + "_results.csv"), std::ios_base::app);
//	  myfile.open("results/rajat31_results.csv", std::ios_base::app);
	  myfile.open("atmosmodd_results.csv", std::ios_base::app);
	 // myfile.open("results/pre2_results.csv", std::ios_base::app);
	  myfile << "\n" << numVecs << "," << size << "," << AvgBlockTime << "," << AvgSingleTime << "\n";
	  myfile.close();
  }
  MPI_Barrier(MPI_COMM_WORLD);
  /*
  FILE * gnuplotpipe = popen("gnuplot -persistant","w");
  fprintf(gnuplotpipe, "plot '-' \n");
  for(int i=0;i<size;i++) {
    fprintf(gnuplotpipe, "%d %lf\n", numMatVecs, blockTime);
  }
  fprintf(gnuplotpipe, "e");
  */
#ifdef EPETRA_MPI
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
#endif

  return EXIT_SUCCESS;
}
