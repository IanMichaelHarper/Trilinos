import pylab as plt
import pandas as pd

df = pd.read_csv("rajat31_results.csv")
num_procs = [2,4,8,16]

plt.figure()
for n in num_procs: 
	#plot block time against block size for different numbers of processors
	plt.plot(df["Block Size (s)"][df["NumProcs"]==n],df["Average Block Time"][df["NumProcs"]==n],'o', linestyle='-', label="Block Time, {0} procs".format(n)) 
	plt.plot(df["Block Size (s)"][df["NumProcs"]==n],df["Average Single Time"][df["NumProcs"]==n],'o', linestyle='--', label="Single Time, {0} procs".format(n)) 
plt.xlabel("Block Size s")
plt.ylabel("Time")
plt.title("Time vs Block size for rajat31.mtx")
plt.legend(loc="upper left", prop={'size':10})
plt.savefig("rajat31_graph.png")

df = pd.read_csv("atmosmodd_results.csv")

plt.figure()
for n in num_procs: 
	#plot block time against block size for different numbers of processors
	plt.plot(df["Block Size (s)"][df["NumProcs"]==n],df["Average Block Time"][df["NumProcs"]==n],'o', linestyle='-', label="Block Time, {0} procs".format(n)) 
	plt.plot(df["Block Size (s)"][df["NumProcs"]==n],df["Average Single Time"][df["NumProcs"]==n],'o', linestyle='--', label="Single Time, {0} procs".format(n)) 
plt.xlabel("Block Size s")
plt.ylabel("Time")
plt.title("Time vs Block size for atmosmodd.mtx")
plt.legend(loc="upper left", prop={'size':10})
plt.savefig("atmosmodd_graph.png")

df = pd.read_csv("pre2_results.csv")

plt.figure()
for n in num_procs: 
	#plot block time against block size for different numbers of processors
	plt.plot(df["Block Size (s)"][df["NumProcs"]==n],df["Average Block Time"][df["NumProcs"]==n],'o', linestyle='-', label="Block Time, {0} procs".format(n)) 
	plt.plot(df["Block Size (s)"][df["NumProcs"]==n],df["Average Single Time"][df["NumProcs"]==n],'o', linestyle='--', label="Single Time, {0} procs".format(n)) 
plt.xlabel("Block Size s")
plt.ylabel("Time")
plt.title("Time vs Block size for pre2.mtx")
plt.legend(loc="upper left", prop={'size':10})
plt.savefig("pre2_graph.png")

